import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:Quotes/models/qutes/quotes_model.dart';
import 'package:Quotes/models/qutes/recieved_notification_model.dart';
import 'package:Quotes/quote.dart';
import 'package:Quotes/repository/apis/api_service.dart';
import 'package:Quotes/repository/repository.dart';
import 'package:Quotes/utils/get_it.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:http/http.dart' as http;

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
    BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject =
    BehaviorSubject<String>();

NotificationAppLaunchDetails notificationAppLaunchDetails;

void requestIOSPermissions() {
  flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          IOSFlutterLocalNotificationsPlugin>()
      ?.requestPermissions(
        alert: true,
        badge: true,
        sound: true,
      );
}

void configureSelectNotificationSubject(BuildContext context) {
  selectNotificationSubject.stream.listen((String payload) async {
    print(payload);
  });
}

init() async {
  notificationAppLaunchDetails =
      await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
  var initializationSettingsIOS = IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
      onDidReceiveLocalNotification:
          (int id, String title, String body, String payload) async {
        didReceiveLocalNotificationSubject.add(ReceivedNotification(
            id: id, title: title, body: body, payload: payload));
      });
  var initializationSettings = InitializationSettings(
      initializationSettingsAndroid, initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
    selectNotificationSubject.add(payload);
  });
}

Future<void> repeatNotification() async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'repeating channel id',
      'repeating channel name',
      'repeating description');
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.periodicallyShow(0, 'repeating title',
      'repeating body', RepeatInterval.EveryMinute, platformChannelSpecifics,
      payload: "bu da bizim yolladigimiz mesaj");
}

Future<void> showDailyAtTime(String time) async {
  var quotes = await getIt<Repository>().getDailyQuotes();
  List<String> listTime = time.split(':');
  Time _time = Time(int.parse(listTime[0]), int.parse(listTime[1]));
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'quoteChannelID', 'Quote', 'Daily quote channel');
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.showDailyAtTime(
      0,
      'Daily quote from Chuck Norris :',
      quotes.value,
      _time,
      platformChannelSpecifics);
}

Future<void> showNotification() async {
  var quotes = await getIt<ApiService>().getRandomQuotes();
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'quoteChannelId', 'Quote', 'Daily quote channel',
      importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
      0, 'Quote from Chuck Norris', quotes.value, platformChannelSpecifics,
      payload: quotes.value);
}

Future onSelectNotification(String payload) async {
  if (payload != null) {
    print("onSelectNotification : " + payload);
  }
}

Future<void> cancelAllNotifications() async {
  await flutterLocalNotificationsPlugin.cancelAll();
}

Future onDidReceiveLocalNotification(
    int id, String title, String body, String payload) {}
