
import 'package:Quotes/repository/apis/api_service.dart';
import 'package:Quotes/repository/repository.dart';
import 'package:Quotes/viewModels/DailyQuoteViewModel.dart';
import 'package:get_it/get_it.dart';

GetIt getIt = GetIt.instance;

void getItInit(){
  getIt.registerLazySingleton<Repository>(() => Repository());
  getIt.registerLazySingleton<ApiService>(() => ApiService());
  getIt.registerLazySingleton<DailyQuoteViewModel>(() => DailyQuoteViewModel());
}

