import 'package:Quotes/main.dart';
import 'package:Quotes/models/qutes/quotes_model.dart';
import 'package:Quotes/repository/apis/api_service.dart';
import 'package:Quotes/repository/repository.dart';
import 'package:Quotes/settings_page.dart';
import 'package:Quotes/utils/get_it.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FavoritesPage extends StatefulWidget {
  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage> {
  Repository _repository = getIt<Repository>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favorities"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingsPage()));
            },
          )
        ],
      ),
      body: FutureBuilder(
        future: getIt<Repository>().getAllQuotes(),
        builder: (context, AsyncSnapshot<List<QuotesModel>> quotelist) {
          if (quotelist.hasData) {
            if (quotelist.data.length > 0) {
              return ListView.builder(
                  itemCount: quotelist.data.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Card(
                        child: ListTile(
                          title: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(quotelist.data[index].value),
                          ),
                          subtitle: Text(
                            DateFormat("dd.MM.yyyy").format(DateTime.parse(
                                quotelist.data[index].createdAt)),
                            textAlign: TextAlign.right,
                          ),
                          trailing: IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () async {
                              var result = await _repository
                                  .deleteQuote(quotelist.data[index].id);
                              if (result == 1) {
                                setState(() {});
                              }
                            },
                          ),
                        ),
                      ),
                    );
                  });
            } else
              return Center(
                child: Text("Empty your favorite list"),
              );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
