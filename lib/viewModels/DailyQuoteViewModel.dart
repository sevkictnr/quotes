
import 'package:Quotes/main.dart';
import 'package:Quotes/models/qutes/quotes_model.dart';
import 'package:Quotes/repository/repository.dart';
import 'package:Quotes/utils/get_it.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum QuoteDailyState {
  idle, loading, loaded, error
}

class DailyQuoteViewModel with ChangeNotifier{
  Repository _repository = getIt<Repository>();
  QuoteDailyState _state;
  QuotesModel _quotesDaily;
  get quotesDaily async{
    return await getDailyQuote();
  }

  get state => _state;
  set state(QuoteDailyState st){
    _state = st;
    notifyListeners();
  }

  Future<QuotesModel> getDailyQuote()async{
    try{
      state = QuoteDailyState.loading;
      _quotesDaily = await _repository.getDailyQuotes();
      print(_quotesDaily.toMap().toString());
      state = QuoteDailyState.loaded;
      return _quotesDaily;
    } catch(e) {
      print( e);
    }
  }

   Future<int> update(QuotesModel quotesModel) async {
    return await _repository.updateQuote(quotesModel);
   }
}