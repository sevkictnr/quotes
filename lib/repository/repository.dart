import 'package:Quotes/models/qutes/quotes_model.dart';
import 'package:Quotes/repository/apis/api_service.dart';
import 'package:Quotes/repository/database/db_helper.dart';
import 'package:Quotes/utils/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Repository {

  DatabaseHelper _databaseHelper = DatabaseHelper();
  ApiService _apiService = getIt<ApiService>();
  SharedPreferences pref;

  Future<QuotesModel> getDailyQuotes() async {
    QuotesModel quote = await _databaseHelper.getDailyQuete();
    print('repository getDailyQuete  gelen sonuc =>  ${quote.toMap()}');
    if (quote != null) {
      print(
          "${DateTime.parse(quote.createdAt).day}  ==  ${DateTime.now().day}");
      if (DateTime.parse(quote.createdAt).day == DateTime.now().day) {
        return quote;
      } else {
        return saveQuote();
      }
    } else {
      return await saveQuote();
    }
  }

  Future<QuotesModel> saveQuote() async {
    var quote = await _apiService.getRandomQuotes();
    var id = await insertQuote(quote);
    print('son eklenen elemanin id si:  $id');
    quote.id = id;
    return quote;
  }

  Future<int> insertQuote(QuotesModel quotesModel) async {
    return await _databaseHelper.insertQuotes(quotesModel);
  }

  Future<int> deleteQuote(int id) async {
    return await _databaseHelper.deleteQuotes(id);
  }

  void closeDB() {
    _databaseHelper.cancel();
  }

  Future<List<QuotesModel>> getAllQuotes() async {
    return await _databaseHelper.getQuotes();
  }

  Future<List<QuotesModel>> searchQuotes(String query) async {
    return await _apiService.searchQuotes(query);
  }

  setQuoteSharedpreferences(QuotesModel quotesModel, bool isfavorited) async {
    var pref = await SharedPreferences.getInstance();
    pref.setInt('id', quotesModel.id);
    pref.setString('value', quotesModel.value);
    pref.setStringList('categories', quotesModel.categories);
    pref.setString('iconUrl', quotesModel.iconUrl);
    pref.setString('createAt', quotesModel.createdAt);
    pref.setBool('isFavorited', isfavorited);
  }

  Future<QuotesModel> getQuoteSharedPreferences() async {
    var pref = await SharedPreferences.getInstance();
    QuotesModel resultQuote = QuotesModel();
    resultQuote.id = pref.getInt('id');
    resultQuote.value = pref.getString('value');
    resultQuote.categories = pref.getStringList('categories');
    resultQuote.iconUrl = pref.getString('iconUrl');
    resultQuote.createdAt = pref.getString('createAt');
    resultQuote.isFavorited = pref.getInt('isFavorited');
    return resultQuote;
  }

  Future<int> updateQuote(QuotesModel quotesModel) async {
    return await _databaseHelper.updateQuotes(quotesModel);
  }

  Future<List<String>> getCategories() async {
    return await _apiService.getCategories();
  }
}
