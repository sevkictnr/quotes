import 'dart:convert';

import 'package:Quotes/models/qutes/quotes_model.dart';
import 'package:Quotes/models/qutes/serach_quotes_model.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
class ApiService {
  final Map<String, String> header = {
    "x-rapidapi-host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com",
    "x-rapidapi-key": "ba8909f054msh4e7dafbbe053557p1de9bajsnff0afa309fdb",
    "accept": "application/json"
  };
  final String BASEURL =
      "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/";
//https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/
  Future<QuotesModel> getRandomQuotes() async {
    String link = BASEURL+"/jokes/random";

    var response = await http.get(link, headers: header);
    return QuotesModel.fromMapAPI(jsonDecode(response.body));
  }

  Future<List<QuotesModel>> searchQuotes(String query) async {
    print("api de ki quiery"+query);
    String link = "$BASEURL/jokes/search?query=$query";
    var jsonResult = await http.get(link, headers: header);
    print(jsonResult.body);
    SearchQuotesModel searchQuotesModel = searchQuotesModelFromJson(jsonResult.body);

    return searchQuotesModel.result;
  }

  Future<List<String>> getCategories() async {
   var response =  await http.get(BASEURL+'/jokes/categories', headers: header);
    List<String> listCategory = List<String>.from(jsonDecode(response.body));
    print("categories:  "+listCategory.toString());
    return listCategory;
  }
}
