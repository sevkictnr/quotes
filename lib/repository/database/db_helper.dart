import 'dart:async';
import 'package:Quotes/models/qutes/quotes_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  final String tableName = "QuotesTable";
  final String columnCategories = "categories";
  final String columnIconUrl = "icon_url";
  final String columnId = "id";
  final String columnValue = "value";
  final String columnCreatedAt = "created_at";
  final String columnUpdatedAt = "updated_at";
  final String columnUrl = "url";
  final String columnIsFavorited = "isFavorited";
  static Database _db;

  Future<Database> get db async {
    if (_db != null)
      return _db;
    else
      return await initDB();
  }

  initDB() async {
    try {
      String dbFolderPath = await getDatabasesPath();
      String dbPath = join(dbFolderPath, "Quotes.db");
      return await openDatabase(dbPath, version: 1, onCreate: _onCreate);
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY AUTOINCREMENT, $columnValue TEXT, $columnCategories TEXT, $columnIconUrl TEXT, $columnCreatedAt TEXT, $columnIsFavorited INTEGER)"); //, $columnUrl TEXT, $columnUpdatedAt TEXT)");
    await db.insert(tableName, quote1);
    await db.insert(tableName, quote2);
  }

  var quote1 = {
    "value":
        "Chuck Norris doesn't use a microwave to pop his popcorn. He simply sits the package on the counter and the kernals jump in fear of a round house kick",
    "categories": [],
    "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    "created_at": "2020-01-05T13:42:28.420821",
    "isFavorited": 0
  };

  var quote2 = {
    "value": "He, Chuck Norris, is awesome!!!",
    "categories": [],
    "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    "created_at": "2020-03-28T13:42:23.484083",
    "isFavorited": 0
  };

  Future<List<QuotesModel>> getQuotes() async {
    var dbClient = await db;
    List<QuotesModel> resultList = [];
    List<Map<String, dynamic>> result = await dbClient
        .rawQuery('SELECT * FROM $tableName WHERE $columnIsFavorited = 1');
    for (var item in result) {
      resultList.add(QuotesModel.fromMap(item));
    }
    return resultList;
  }

  Future<int> insertQuotes(QuotesModel quotesModel) async {
    try {
      var dbClient = await db;
      int result = await dbClient.insert(tableName, quotesModel.toMap());
      return result;
    } catch (e) {
      return null;
    }
  }

  Future<int> updateQuotes(QuotesModel quotesModel) async {
    print('update db de gelen quote = ' + quotesModel.toMap().toString());
    var dbClient = await db;
    return await dbClient.rawUpdate(
        'UPDATE $tableName SET $columnIsFavorited = ${quotesModel.isFavorited} WHERE $columnId = ${quotesModel.id}');
  }

  Future<int> deleteQuotes(int id) async {
    var dbClient = await db;
    return await dbClient
        .rawDelete('DELETE FROM $tableName WHERE $columnId = ?', [id]);
  }

  void cancel() async {
    await _db.close();
  }

  Future<QuotesModel> getQuete(String id) async {
    var dbCleint = await db;
    try {
      List<Map<String, dynamic>> quoteJsont =
          await dbCleint.query(tableName, where: columnId, whereArgs: [id]);
      if (quoteJsont.length > 0)
        return QuotesModel.fromMap(quoteJsont[0]);
      else
        return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<QuotesModel> getDailyQuete() async {
    var dbCleint = await db;
    try {
      List<Map<String, dynamic>> quoteMap = await dbCleint.rawQuery(
          "SELECT * FROM $tableName ORDER BY date($columnCreatedAt) DESC Limit 1");
      print('getDailyQuete calisti');
      if (quoteMap[0] != null)
        return QuotesModel.fromMap(quoteMap[0]);
      else
        return null;
    } catch (e) {
      print("dbhelper da getDailyQuote : " + e.toString());
      return null;
    }
  }

}
