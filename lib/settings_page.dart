import 'package:Quotes/repository/repository.dart';
import 'package:Quotes/utils/get_it.dart';
import 'package:Quotes/utils/notification_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String _time = "10:00:00";
  String _dropdownValue;
  SharedPreferences _prefs;
  List<String> categories = List<String>();
  Repository _repository = getIt<Repository>();
  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) => _prefs = value);
    getCategories();
  }

   Future<void> getCategories()async{
    categories =  await _repository.getCategories();
    setState(() {

    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notification Settings"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text("Select e time of the day :"),
                RaisedButton(
                  child: Text(_time),
                  onPressed: () {
                    _showTimePicker();
                  },
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  "Select a Category : ",
                ),
                DropdownButton<String>(
                  items: _getItems(),
                  value: _dropdownValue,
                  hint: Text('Select'),
                  onChanged: (value) {
                    setState(() {
                      _dropdownValue = value;
                    });
                  },
                ),
              ],
            ),
            Expanded(
              child: SizedBox(),
            ),
            Container(
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      padding: EdgeInsets.all(10),
                      color: Colors.red,
                      child: Text("Stop",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                      onPressed: () async {
                        cancelAllNotifications();
                      },
                    ),
                  ),
                  Expanded(
                    child: RaisedButton(
                      padding: EdgeInsets.all(10),
                      color: Colors.green,
                      child: Text("Start",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                      onPressed: () async {
                        showDailyAtTime(_time);
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showTimePicker() async {
    var _picker =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    setState(() {
      _time = "${_picker.hour}:${_picker.minute}:${_picker.minute}";
    });
  }

  List<DropdownMenuItem<String>> _getItems() {
    return categories
        .map((e) => DropdownMenuItem<String>(
      child: Text(e),
      value: e,
    )).toList();
  }
}
