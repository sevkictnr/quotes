

import 'dart:convert';

import 'package:Quotes/models/qutes/quotes_model.dart';

SearchQuotesModel searchQuotesModelFromJson(String str) => SearchQuotesModel.fromMap(json.decode(str));

String searchQuotesModelToJson(SearchQuotesModel data) => json.encode(data.toMap());

class SearchQuotesModel {
  int total;
  List<QuotesModel> result;

  SearchQuotesModel({
    this.total,
    this.result,
  });

  factory SearchQuotesModel.fromMap(Map<String, dynamic> json) => SearchQuotesModel(
    total: json["total"] == null ? null : json["total"],
    result: json["result"] == null ? null : List<QuotesModel>.from(json["result"].map((x) => QuotesModel.fromMapAPI(x))),
  );

  Map<String, dynamic> toMap() => {
    "total": total == null ? null : total,
    "result": result == null ? null : List<QuotesModel>.from(result.map((x) => x.toMap())),
  };
}