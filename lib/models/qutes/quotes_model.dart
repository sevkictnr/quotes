import 'dart:convert';

QuotesModel quotesModelFromJson(String str) =>
    QuotesModel.fromMap(json.decode(str));

String quotesModelToJson(QuotesModel data) => json.encode(data.toMap());

class QuotesModel {
  List<dynamic> categories;
  String createdAt;
  String iconUrl;
  int id;
  String value;
  int isFavorited;

  QuotesModel(
      {this.categories,
      this.iconUrl,
      this.id,
      this.createdAt,
      this.value,
      this.isFavorited});

  factory QuotesModel.fromMap(Map<String, dynamic> json) {
    return QuotesModel(
        categories: json["categories"] == null
            ? null
            : List<String>.from(json["categories"].map((x) => x)),
        iconUrl: json["icon_url"] == null ? null : json["icon_url"],
        id: json["id"] == null ? null : json["id"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
        value: json["value"] == null ? null : json["value"],
        isFavorited: json["isFavorited"] == null ? 0 : json["isFavorited"]);
  }

  factory QuotesModel.fromMapAPI(Map<String, dynamic> json) {
    return QuotesModel(
        categories: json["categories"] == null
            ? null
            : List<String>.from(json["categories"].map((x) => x)),
        iconUrl: json["icon_url"] == null ? null : json["icon_url"],
        id: 0,
        createdAt: DateTime.now().toIso8601String(),
        //json["created_at"] == null ? null : json["created_at"],
        value: json["value"] == null ? null : json["value"],
        isFavorited: 0);
  }

  Map<String, dynamic> toMap() => {
        "categories": categories == null
            ? null
            : List<String>.from(categories.map((x) => x)),
        "icon_url": iconUrl == null ? null : iconUrl,
        "id": id == null ? null : id,
        "created_at": createdAt == null ? null : createdAt,
        "value": value == null ? null : value,
        "isFavorited": isFavorited == null ? 0 : isFavorited
      };
}
