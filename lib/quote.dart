import 'package:Quotes/main.dart';
import 'package:Quotes/models/qutes/quotes_model.dart';
import 'package:Quotes/repository/repository.dart';
import 'package:Quotes/settings_page.dart';
import 'package:Quotes/utils/get_it.dart';
import 'package:Quotes/viewModels/DailyQuoteViewModel.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class QuotePage extends StatefulWidget {
  @override
  _QuotePageState createState() => _QuotePageState();
}

class _QuotePageState extends State<QuotePage> {
  DailyQuoteViewModel _dailyQuoteViewModel = getIt<DailyQuoteViewModel>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _dailyQuoteViewModel.getDailyQuote(),
      builder: (context, AsyncSnapshot<QuotesModel> asyncSnapshot) {
        print('dateTime.now ' + DateTime.now().toString());
        if (asyncSnapshot.hasData) {
          return _showDailyQuote(asyncSnapshot.data);
        } else if (asyncSnapshot.hasError) {
          return Container(
            child: Center(
              child: Text("Error..."),
            ),
          );
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }

  Scaffold _showDailyQuote(QuotesModel _quotesModel) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Quotes"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingsPage()));
            },
          )
        ],
      ),
      body: _quotesModel == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Image.asset(
                      "assets/images/chucknorris.png",
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Text(
                      "\"${_quotesModel.value}\"",
                      style: TextStyle(
                          color: Colors.blueGrey,
//                          fontSize: 20,
//                          fontFamily: 'Courier-New',
//                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  )
                ],
              ),
            ),
      floatingActionButton: _quotesModel == null
          ? null
          : Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FloatingActionButton(
                  heroTag: 'flt2',
                  child: Icon(Icons.share),
                  onPressed: () {
                    Share.share(_quotesModel.value);
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                FloatingActionButton(
                  heroTag: "flt1",
                  backgroundColor: Colors.cyanAccent,
                  child: _quotesModel.isFavorited == 1
                      ? Icon(
                          Icons.favorite,
                          color: Colors.pinkAccent,
                        )
                      : Icon(Icons.favorite_border),
                  onPressed: () async {
                    if (_quotesModel.isFavorited == 0) {
                      _quotesModel.isFavorited = 1;
                      await _dailyQuoteViewModel.update(_quotesModel);
                      setState(() {});
                    } else {
                      _quotesModel.isFavorited = 0;
                      await _dailyQuoteViewModel.update(_quotesModel);
                      setState(() {});
                    }
                  },
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
    );
  }
}
