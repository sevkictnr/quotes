import 'package:Quotes/main.dart';
import 'package:Quotes/models/qutes/quotes_model.dart';
import 'package:Quotes/repository/repository.dart';
import 'package:Quotes/settings_page.dart';
import 'package:Quotes/utils/get_it.dart';
import 'package:flutter/material.dart';

class SearchQuote extends StatefulWidget {
  @override
  _SearchQuoteState createState() => _SearchQuoteState();
}

class _SearchQuoteState extends State<SearchQuote> {
  TextEditingController searchController = TextEditingController();
  List<QuotesModel> resultQuotes = [];
  String query = "";
  Repository _repository = getIt<Repository>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Column(
            children: <Widget>[
              Text("Search Quote"),
            ],
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SettingsPage()));
              },
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Card(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Search a quote',
                            ),
                            onChanged: (value) {
                              query = value;
                            },
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.search),
                          onPressed: () async {
                            resultQuotes =
                                await _repository.searchQuotes(query);
                            _listItemsCreator();
                            setState(() {});
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
              Container(
                  child: resultQuotes.length == 0
                      ? null
                      : ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: resultQuotes.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Card(
                                  child: ListTile(
                                title: Text(resultQuotes[index].value),
                                trailing: IconButton(
                                    icon: resultQuotes[index].isFavorited == 1
                                        ? Icon(
                                            Icons.favorite,
                                            color: Colors.pinkAccent,
                                          )
                                        : Icon(Icons.favorite_border),
                                    onPressed: () async {
                                      await _favoriteSelection(index);
                                    }),
                              )),
                            );
                          }
                          )
              )
            ],
          ),
        ));
  }

  Future _favoriteSelection(int index) async {
    if (resultQuotes[index].isFavorited == 0) {
      resultQuotes[index].isFavorited = 1;
      resultQuotes[index].id = null;
      int gelencevap = await _repository.insertQuote(resultQuotes[index]);
      resultQuotes[index].id = gelencevap;
    } else {
      resultQuotes[index].isFavorited = 0;
      await _repository.deleteQuote(resultQuotes[index].id);
    }
    setState(() {});
  }

  List<ListTile> _listItemsCreator() {
    if (resultQuotes != null)
      return resultQuotes
          .map((e) => ListTile(
                title: Text(e.value),
              ))
          .toList();
  }
}
